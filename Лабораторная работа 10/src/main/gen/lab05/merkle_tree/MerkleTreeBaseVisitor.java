// Generated from C:/Users/Daria/Downloads/sarafan-RestController/sarafan-RestController/src/main/java/lab05/merkle_tree\MerkleTree.g4 by ANTLR 4.9
package lab05.merkle_tree;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

/**
 * This class provides an empty implementation of {@link MerkleTreeVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class MerkleTreeBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements MerkleTreeVisitor<T> {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitR(MerkleTreeParser.RContext ctx) { return visitChildren(ctx); }
}