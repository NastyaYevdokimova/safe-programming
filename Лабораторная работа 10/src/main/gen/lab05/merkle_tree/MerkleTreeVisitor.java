// Generated from C:/Users/Daria/Downloads/sarafan-RestController/sarafan-RestController/src/main/java/lab05/merkle_tree\MerkleTree.g4 by ANTLR 4.9
package lab05.merkle_tree;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MerkleTreeParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MerkleTreeVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MerkleTreeParser#r}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitR(MerkleTreeParser.RContext ctx);
}