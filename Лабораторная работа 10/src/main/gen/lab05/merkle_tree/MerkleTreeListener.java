// Generated from C:/Users/Daria/Downloads/sarafan-RestController/sarafan-RestController/src/main/java/lab05/merkle_tree\MerkleTree.g4 by ANTLR 4.9
package lab05.merkle_tree;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MerkleTreeParser}.
 */
public interface MerkleTreeListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MerkleTreeParser#r}.
	 * @param ctx the parse tree
	 */
	void enterR(MerkleTreeParser.RContext ctx);
	/**
	 * Exit a parse tree produced by {@link MerkleTreeParser#r}.
	 * @param ctx the parse tree
	 */
	void exitR(MerkleTreeParser.RContext ctx);
}