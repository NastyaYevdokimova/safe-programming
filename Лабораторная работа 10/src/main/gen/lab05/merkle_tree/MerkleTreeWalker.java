package lab05.merkle_tree;

public class MerkleTreeWalker extends MerkleTreeBaseListener {
    public void enterR(MerkleTreeParser.RContext ctx ) {
        System.out.println( "Entering R : " + ctx.ID().getText() );
    }

    public void exitR(MerkleTreeParser.RContext ctx ) {
        System.out.println( "Exiting R" );
    }
}
