package lab05.merkle_tree.controller;

import lab05.merkle_tree.exceptions.NotFoundException;
import lab05.merkle_tree.server.MerkleTrees;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("client")
public class ClientController {
    private int counter = 4;
    List<String> list = new ArrayList<String>(){{
        add("Student");
        add("Ecologist");
        add("A fighter to clean up the ocean"); }};

    private List<Map<String, String>> client = new ArrayList<Map<String, String>>() {{
        add(new HashMap<String, String>() {{ put("id", "1"); put("Daria", new MerkleTrees(list).getRoot()); }});
        add(new HashMap<String, String>() {{ put("id", "2"); put("Alex", new MerkleTrees(list).getRoot()); }});
        add(new HashMap<String, String>() {{ put("id", "3"); put("Yehor", new MerkleTrees(list).getRoot()); }});
    }};

    @GetMapping
    public List<Map<String, String>> list() {
        return client;
    }

    @GetMapping("{id}")
    public Map<String, String> getOne(@PathVariable String id) {
        return getMessage(id);
    }

    private Map<String, String> getMessage(@PathVariable String id) {
        return client.stream()
                .filter(client -> client.get("id").equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public Map<String, String> create(@RequestBody Map<String, String> message) {
        message.put("id", String.valueOf(counter++));

        client.add(message);

        return message;
    }

    @PutMapping("{id}")
    public Map<String, String> update(@PathVariable String id, @RequestBody Map<String, String> message) {
        Map<String, String> messageFromDb = getMessage(id);

        messageFromDb.putAll(message);
        messageFromDb.put("id", id);

        return messageFromDb;
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        Map<String, String> message = getMessage(id);

        client.remove(message);
    }
}
