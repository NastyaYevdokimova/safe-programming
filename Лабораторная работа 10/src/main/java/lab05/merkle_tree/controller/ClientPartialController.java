package lab05.merkle_tree.controller;

import lab05.merkle_tree.exceptions.NotFoundException;
import lab05.merkle_tree.server.MerkleTrees;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("clientPart")
public class ClientPartialController {
    private int counter = 4;

    private List<Map<String, String>> client = new ArrayList<Map<String, String>>() {{
        add(new HashMap<String, String>() {{ put("id", "Daria");}});
        add(new HashMap<String, String>() {{ put("id", "Alex");}});
        add(new HashMap<String, String>() {{ put("id", "Yehor"); }});
    }};

    @GetMapping
    public List<Map<String, String>> list() {
        return client;
    }

    @GetMapping("{id}")
    public Map<String, String> getOne(@PathVariable String id) {
        return getMessage(id);
    }

    private Map<String, String> getMessage(@PathVariable String id) {
        return client.stream()
                .filter(client -> client.get("id").equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public Map<String, String> create(@RequestBody Map<String, String> message) {
        message.put("id", String.valueOf(counter++));

        client.add(message);

        return message;
    }

    @PutMapping("{id}")
    public Map<String, String> update(@PathVariable String id, @RequestBody Map<String, String> message) {
        Map<String, String> messageFromDb = getMessage(id);

        messageFromDb.putAll(message);
        messageFromDb.put("id", id);

        return messageFromDb;
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        Map<String, String> message = getMessage(id);

        client.remove(message);
    }
}
