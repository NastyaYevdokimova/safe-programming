﻿#include <iostream>
using namespace std;
//определяем функции
void showText1(char str[])//функция принимает строку, как массив
{
    cout << str << endl;
}

void showText2(char* str)//указатель *str будет указывать на адрес первого символа в строке
{
    cout << str << endl;
}

void showText3(char(&str)[150])// адрес строки из 150-ти символов
{
    cout << str << endl;
}
  

void eq(int password) {
    if (password = '1234') {
        cout << "True" << endl;
    }
    else {
        cout << "False" << endl;
    }


}



int main()
{
    setlocale(LC_ALL, "rus");


    char str1[] = "str1 - передаем, как массив в функцию void showText1 (char str[]);";
    showText1(str1);

    cout << endl;

    char str2[] = "str2 - передаем, в функцию void showText2 (char *str); используя указатель.";
    showText2(str2);

    cout << endl;

    char str3[150] = "str3 - передаем, в функцию void showText3 (char &str[]);\nТут используем адрес строки.";
    showText3(str3);

    cout << endl;
    cout << "Input password:" << endl;
    int password;
    cin >> password;
    eq(password);


    return 0;
}